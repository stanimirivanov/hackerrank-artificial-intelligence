package org.sonofizanagi.algo.statisticsandmachinelearning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import weka.classifiers.Classifier;
import weka.classifiers.functions.LinearRegression;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.MultiFilter;
import weka.filters.unsupervised.attribute.AddExpression;

/**
 * Charlie wants to purchase office-space. He does a detailed survey of the
 * offices and corporate complexes in the area, and tries to quantify a lot of
 * factors, such as the distance of the offices from residential and other
 * commercial areas, schools and workplaces; the reputation of the construction
 * companies and builders involved in constructing the apartments; the distance
 * of the offices from highways, freeways and important roads; the facilities
 * around the office space and so on.
 * 
 * Each of these factors are quantified, normalized and mapped to values on a
 * scale of 0 to 1. Charlie then makes a table. Each row in the table
 * corresponds to Charlie's observations for a particular house. If Charlie has
 * observed and noted F features, the row contains F values separated by a
 * single space, followed by the office-space price in dollars/square-foot. If
 * Charlie makes observations for H houses, his observation table has (F+1)
 * columns and H rows, and a total of (F+1) * H entries.
 * 
 * Charlie does several such surveys and provides you with the tabulated data.
 * At the end of these tables are some rows which have just F columns (the price
 * per square foot is missing). Your task is to predict these prices. F can be
 * any integer number between 1 and 5, both inclusive.
 * 
 * There is one important observation which Charlie has made.
 * 
 * The prices per square foot, are (approximately) a polynomial function of the
 * features in the observation table. This polynomial always has an order less
 * than 4
 *
 * @see https://www.hackerrank.com/challenges/predicting-office-space-price
 */
public class PolynomialRegression {

	public static List<Double> classifyOffice(Classifier classifier,
			List<Instance> instances) {
		List<Double> result = new ArrayList<Double>();
		for (Instance instance : instances) {
			try {
				double price = classifier.classifyInstance(instance);
				result.add(price);
			} catch (Exception ex) {
				System.out.println("Can't clacify: " + ex.getMessage());
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		String line1 = in.nextLine();
		StringTokenizer st1 = new StringTokenizer(line1, " ");
		int f = Integer.parseInt(st1.nextToken());
		int n = Integer.parseInt(st1.nextToken());
		
		FastVector wekaAttributes = new FastVector(f + 1);
		for (int att = 0; att < f; att++) {
			String featureName = "a" + att;
			wekaAttributes.addElement(new Attribute(featureName, att));
		}
		wekaAttributes.addElement(new Attribute("price", f + 1));
		
		Instances data = new Instances("Rel", wekaAttributes, n);
		data.setClassIndex(f);
		
		// Create the training instances
		for (int i = 0; i < n; i++) {
			Instance instance = new Instance(f + 1);
			int featureIndex = 0;
			String line = in.nextLine();
			StringTokenizer st = new StringTokenizer(line);
			while (st.hasMoreElements()) {
				Double featureValue = Double.parseDouble(st.nextElement().toString());
				instance.setValue(featureIndex, featureValue);
				featureIndex++;
			}
			instance.setDataset(data);
			data.add(instance);
		}
		
		
		
		// create the filter
		List<AddExpression> filters = new ArrayList<AddExpression>();
		MultiFilter multiFilter = new MultiFilter();
		for (int att = 0; att < f; att++) {
			String featureName = "a" + att;
			for (int i = 2; i <= 3; i++) {
				AddExpression addExpression = new AddExpression();
//				try {
//					addExpression.setInputFormat(data);
//				} catch (Exception ex) {
//					System.out.println("Can't set input: " + ex.getMessage());
//				}
				addExpression.setExpression(featureName + "^" + i);
				filters.add(addExpression);
			}
		}
		multiFilter.setFilters(filters.toArray(new AddExpression[filters.size()]));
		
		try {
			multiFilter.setInputFormat(data);
		} catch (Exception ex) {
			System.out.println("Can't set input: " + ex.getMessage());
		}
		
		
//		Instances filteredTrainingSet = null;
//		try {
//			filteredTrainingSet = Filter.useFilter(data, multiFilter);
//		} catch (Exception ex) {
//			System.out.println("Can't create filter: " + ex.getMessage());
//		}
		
		System.out.println(data);
		System.out.println(Arrays.asList(multiFilter.getOptions()));
		
		// Build the liner regression model
		
		 FilteredClassifier classifier = new FilteredClassifier();
		 classifier.setFilter(multiFilter);
		 classifier.setClassifier(new LinearRegression());
		 try {
			classifier.buildClassifier(data);
		} catch (Exception ex) {
			System.out.println("Can't build classifier: " + ex.getMessage());
		}
		 
//		LinearRegression classifier = new LinearRegression();
//		try {
//			classifier.buildClassifier(filteredTrainingSet);
//		} catch (Exception ex) {
//			System.out.println("Can't build model: " + ex.getMessage());
//		}
		
		// Create the test instances
		List<Instance> testInstances = new ArrayList<Instance>();

		String line2 = in.nextLine();
		int t = Integer.parseInt(line2);
		for (int i = 0; i < t; i++) {
			Instance instance = new Instance(f);
			int featureIndex = 0;
			String line = in.nextLine();
			StringTokenizer st = new StringTokenizer(line);
			while (st.hasMoreElements()) {
				Double featureValue = Double.parseDouble(st.nextElement().toString());
				instance.setValue(featureIndex, featureValue);
				featureIndex++;
			}
			testInstances.add(instance);
		}

		in.close();

		// Classify and print result
		List<Double> prices = classifyOffice(classifier, testInstances);
		for (Double price : prices) {
			System.out.println(price);
		}
	}
}
