package org.sonofizanagi.algo.statisticsandmachinelearning;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import weka.classifiers.functions.LinearRegression;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

/**
 * Charlie wants to buy a house. He does a detailed survey of the houses in the
 * area, and tries to quantify a lot of factors, such as the distance of the
 * houses from commercial areas, schools and workplaces; the reputation of the
 * construction companies and builders involved in constructing the apartments;
 * the distance of the houses from highways, freeways and important roads; the
 * size of the house; the facilities offered by the complex, and so on.
 *
 * Each of these factors are quantified, normalized and mapped to values on a
 * scale of 0 to 1. Charlie then makes a table. Each row in the table
 * corresponds to Charlie's observations for a particular house. If Charlie has
 * observed and noted F features, the row contains F comma separated values,
 * followed by the house price in dollars per square foot at the end. If Charlie
 * makes observations for H houses, his observation table has (F+1) columns and
 * H rows, and a total of (F+1) * H entries.
 *
 * Unfortunately, he was not able to get the pricing of some houses and he wants
 * your help to give out an estimate of the pricing of the house.
 *
 * There is one important observation which Charlie has made.
 *
 * The prices per square foot, are (approximately) a linear function of the
 * features in the observation table. For the purpose of prediction, you need to
 * figure out this linear function.
 *
 * @see https://www.hackerrank.com/challenges/predicting-house-prices
 */
public class MultivariateLinearRegression {
		
	public static List<Double> classifyHouses(LinearRegression model, List<Instance> instances) {
		List<Double> result = new ArrayList<Double>();
		for (Instance instance : instances) {
			try {
				double price = model.classifyInstance(instance);
				result.add(price);
			} catch (Exception ex) {
				System.out.println("Can't clacify instance: " + ex.getMessage());
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		String line1 = in.nextLine();
		StringTokenizer st1 = new StringTokenizer(line1, " ");
		int f = Integer.parseInt(st1.nextToken());
		int n = Integer.parseInt(st1.nextToken());

		FastVector wekaAttributes = new FastVector(f + 1);
		for (int att = 0; att < f; att++) {
			wekaAttributes.addElement(new Attribute("Attribute" + att, att));
		}
		wekaAttributes.addElement(new Attribute("Price", f + 1));

		Instances trainingSet = new Instances("Rel", wekaAttributes, 10);
		// Make the "Price" attribute be the class
		trainingSet.setClassIndex(f);

		// Create the training instances
		for (int i = 0; i < n; i++) {
			Instance instance = new Instance(f + 1);
			int featureIndex = 0;
			String line = in.nextLine();
			StringTokenizer st = new StringTokenizer(line);
			while (st.hasMoreElements()) {
				Double featureValue = Double.parseDouble(st.nextElement().toString());
				instance.setValue(featureIndex, featureValue);
				featureIndex++;
			}
			instance.setDataset(trainingSet);
			trainingSet.add(instance);
		}
		
		// Build the liner regression model
		LinearRegression model = new LinearRegression();
		try {
			model.buildClassifier(trainingSet);
		} catch (Exception ex) {
			System.out.println("Can't build model: " + ex.getMessage());
		}

		// Create the test instances
		List<Instance>  testInstances = new ArrayList<Instance>();
		
		String line2 = in.nextLine();
		int t = Integer.parseInt(line2);
		for (int i = 0; i < t; i++) {
			Instance instance = new Instance(f);
			int featureIndex = 0;
			String line = in.nextLine();
			StringTokenizer st = new StringTokenizer(line);
			while (st.hasMoreElements()) {
				Double featureValue = Double.parseDouble(st.nextElement().toString());
				instance.setValue(featureIndex, featureValue);
				featureIndex++;
			}
			testInstances.add(instance);
		}
		
		in.close();
		
		//  Classify and print result
		List<Double> prices = classifyHouses(model, testInstances);
		for (Double price : prices) {
			System.out.println(price);
		}
	}

}
