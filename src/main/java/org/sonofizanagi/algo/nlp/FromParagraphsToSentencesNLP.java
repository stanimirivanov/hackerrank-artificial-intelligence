package org.sonofizanagi.algo.nlp;

import java.io.Reader;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;

/**
 * Sentence segmentation, means, to split a given paragraph of text into
 * sentences, by identifying the sentence boundaries. In many cases, a full stop
 * is all that is required to identify the end of a sentence, but the task is
 * not all that simple.
 * 
 * This is an open ended challenge to which there are no perfect solutions. Try
 * to break up given paragraphs into text into individual sentences. Even if you
 * don't manage to segment the text perfectly, the more sentences you identify
 * and display correctly, the more you will score.
 * 
 * Abbreviations: Dr. W. Watson is amazing. In this case, the first and second
 * "." occurs after Dr (Doctor) and W (initial in the person's name) and should
 * not be confused as the end of the sentence.
 * 
 * Sentences enclosed in quotes:
 * "What good are they? They're led about just for show!" remarked another. All
 * of this, should be identified as just one sentence.
 * 
 * Questions and exclamations: Who is it? -This is a question. This should be
 * identified as a sentence. I am tired!: Something which has been exclaimed.
 * This should also be identified as a sentence.
 *
 * @see https://www.hackerrank.com/challenges/from-paragraphs-to-sentences
 */
public class FromParagraphsToSentencesNLP {

	public static List<String> toSentences(String paragraph) {
		Reader reader = new StringReader(paragraph);
		String options = "invertible=true";
		PTBTokenizer ptbt = new PTBTokenizer(reader, new CoreLabelTokenFactory(), options);
		for (CoreLabel label; ptbt.hasNext();) {
			label = (CoreLabel) ptbt.next();
			System.out.println(label);
		}

		List<String> sentenceList = new LinkedList<String>();
		return sentenceList;
	}
	
	public static List<String> toSentencesRegEx(String paragraph) {
		List<String> sentenceList = new LinkedList<String>();
		String regex = "[^.!?\\s][^.!?]*(?:[.!?](?!['\"]?\\s|$)[^.!?]*)*[.!?]?['\"]?(?=\\s|$)";
		Pattern re = Pattern.compile(regex, Pattern.MULTILINE | Pattern.COMMENTS);
	    Matcher reMatcher = re.matcher(paragraph);
	    while (reMatcher.find()) {
	    	sentenceList.add(reMatcher.group());
	    }
		return sentenceList;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String paragraph = in.nextLine();
		in.close();

		printResult(paragraph);
	}

	private static void printResult(String paragraph) {
		List<String> sentenceList = toSentences(paragraph);
		for (String sentence : sentenceList) {
			System.out.println(sentence);
		}
	}
}
