package org.sonofizanagi.algo.botbuilding

/**
 * The goal of Artificial Intelligence is to create a rational agent (Artificial Intelligence 1.1.4).
 * An agent gets input from the environment through sensors and acts on the environment with actuators.
 * In this challenge, you will program a simple bot to perform the correct actions based on environmental input.
 *
 * Meet the bot MarkZoid. It's a cleaning bot whose sensor is a head mounted camera and whose actuators
 * are the wheels beneath it. It's used to clean the floor.
 *
 * The bot here is positioned at the top left corner of a 5*5 grid. Your task is to move the bot to clean
 * all the dirty cells.
 *
 * @see https://www.hackerrank.com/challenges/botclean
 */
object BotClean {

  def nextMove(row: Int, col: Int, board: Array[Array[Char]]): Option[String] = {
    val positions = search('d', board)
    if (positions.isEmpty) None
    else {
      val position = nearest(row, col, positions)
      position match {
        case (rd, cd) if (rd < row)               => Some("UP")
        case (rd, cd) if (rd > row)               => Some("DOWN")
        case (rd, cd) if (cd < col)               => Some("LEFT")
        case (rd, cd) if (cd > col)               => Some("RIGHT")
        case (rd, cd) if (cd == col && rd == row) => Some("CLEAN")
      }
    }

  }

  def nearest(row: Int, col: Int, positions: List[(Int, Int)]): (Int, Int) =
    positions reduce { (cell1, cell2) =>
      val dist1 = math.abs(cell1._1 - row) + math.abs(cell1._2 - col)
      val dist2 = math.abs(cell2._1 - row) + math.abs(cell2._2 - col)
      if (dist1 < dist2) cell1 else cell2
    }

  def search[T](target: T, arr: Array[Array[T]]): List[(Int, Int)] =
    arr.indices.collect {
      case k if arr(k).contains(target) =>
        (k, arr(k).indexWhere(_ == target))
    }.toList

  def main(args: Array[String]) = {
    val lines = io.Source.stdin.getLines().toList
    val x_y = lines.head.split(" ")
    val row = x_y(0).toInt
    val col = x_y(1).toInt
    val board = lines.tail.toArray.map(_.toArray)

    val move = nextMove(row, col, board)
    move match {
      case Some(m) => println(m)
      case None    => ()
    }
  }
}