package org.sonofizanagi.algo.botbuilding

/**
 * In this version of "Bot saves princess", Princess Peach and bot's position are randomly set.
 * Can you save the princess?
 *
 * @see https://www.hackerrank.com/challenges/saveprincess2
 */
object BotSavesPrincess2 {

  def nextMove(player: Int, r: Int, c: Int, grid: Array[Array[Char]]): String = {
    val princess = search('p', grid)
    val pr = princess.head
    val pc = princess.tail.head

    if (pr < r) "UP"
    else if (pr > r) "DOWN"
    else if (pc < c) "LEFT"
    else if (pc > c) "RIGHT"
    else ""
  }

  def search[T](target: T, arr: Array[Array[T]]): List[Int] =
    arr.indices.collectFirst {
      case k if arr(k).contains(target) =>
        List(k, arr(k).indexWhere(_ == target))
    }.getOrElse(Nil)

  def main(args: Array[String]) = {
    val lines = io.Source.stdin.getLines().toList
    val n = lines.head.toInt
    val x_y = lines.tail.head.split(" ")
    val r = x_y(0).toInt
    val c = x_y(1).toInt
    val grid = lines.drop(2).toArray.map(_.toArray)

    val move = nextMove(n, r, c, grid)
    println(move)
  }

}