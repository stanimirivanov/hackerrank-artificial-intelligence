package org.sonofizanagi.algo.botbuilding

/**
 * Princess Peach is trapped in one of the four corners of a square grid.
 * You are in the center of the grid and can move one step at a time in
 * any of the four directions. Can you rescue the princess?
 *
 * @see https://www.hackerrank.com/challenges/saveprincess
 */
object BotSavesPrincess {

  def pathtoPrincess(n: Int, grid: Array[Array[Char]]): List[String] = {
    val (pr, pc) =
      if (grid(0)(0) == 'p') (0, 0)
      else if (grid(0)(n - 1) == 'p') (0, n - 1)
      else if (grid(n - 1)(0) == 'p') (n - 1, 0)
      else (n - 1, n - 1)

    val (mr, mc) = (n / 2, n / 2)

    val up = (mr - 1 to pr by -1).map(i => "UP").toList
    val down = (mr + 1 to pr).map(i => "DOWN").toList
    val left = (mc - 1 to pc by -1).map(i => "LEFT").toList
    val right = (mc + 1 to pc).map(i => "RIGHT").toList

    up ++ down ++ left ++ right
  }

  def displayPathtoPrincess(n: Int, grid: Array[String]): Unit = {
    val moves = pathtoPrincess(n, grid.map(_.toArray))
    moves foreach { move =>
      println(move)
    }
  }

  def main(args: Array[String]) = {
    val lines = io.Source.stdin.getLines().toList
    val n = lines.head.toInt
    val grid = lines.tail.toArray

    displayPathtoPrincess(n, grid)
  }
}