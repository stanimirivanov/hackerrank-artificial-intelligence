package org.sonofizanagi.algo.nlp

/**
 * Sentence segmentation, means, to split a given paragraph of text into
 * sentences, by identifying the sentence boundaries. In many cases, a full stop
 * is all that is required to identify the end of a sentence, but the task is
 * not all that simple.
 *
 * This is an open ended challenge to which there are no perfect solutions. Try
 * to break up given paragraphs into text into individual sentences. Even if you
 * don't manage to segment the text perfectly, the more sentences you identify
 * and display correctly, the more you will score.
 *
 * Abbreviations: Dr. W. Watson is amazing. In this case, the first and second
 * "." occurs after Dr (Doctor) and W (initial in the person's name) and should
 * not be confused as the end of the sentence.
 *
 * Sentences enclosed in quotes:
 * "What good are they? They're led about just for show!" remarked another. All
 * of this, should be identified as just one sentence.
 *
 * Questions and exclamations: Who is it? -This is a question. This should be
 * identified as a sentence. I am tired!: Something which has been exclaimed.
 * This should also be identified as a sentence.
 *
 * @see https://www.hackerrank.com/challenges/from-paragraphs-to-sentences
 */
object FromParagraphsToSentences {

  def toSentences(paragraph: String): List[String] = {
    def loop(paragraph: String, acc: List[String]): List[String] =
      if (paragraph == "") acc
      else {
        if (paragraph.startsWith("\"")) {
          val qmIndex = paragraph.indexOf('"', 1)
          val qmSplit = paragraph.splitAt(qmIndex)
          val punctuation = qmSplit._2 indexWhere { c => c == '.' || c == '!' || c == '?' }
          val split = paragraph.splitAt(qmIndex + punctuation + 1)
          loop(split._2.trim, split._1.trim :: acc)
        } else {
          val punctuation = paragraph indexWhere { c => c == '.' || c == '!' || c == '?' }
          val split = paragraph.splitAt(punctuation + 1)
          loop(split._2.trim, split._1.trim :: acc)
        }
      }
    
    loop(paragraph, Nil).reverse
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines().toList
    printResult(lines.head)
  }

  def printResult(paragraph: String): Unit = {
    val result = toSentences(paragraph)
    result foreach {
      s => println(s)
    }
  }
}
