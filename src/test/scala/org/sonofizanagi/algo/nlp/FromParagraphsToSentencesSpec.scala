package org.sonofizanagi.algo.nlp

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import scala.collection.JavaConversions

@RunWith(classOf[JUnitRunner])
class FromParagraphsToSentencesSpec extends FlatSpec {

  "From Paragraphs to Sentences" should "pass test case 1" in {
    val input = "In the third category he included those Brothers (the majority) who saw nothing in Freemasonry but the external forms and ceremonies, and prized the strict performance of these forms without troubling about their purport or significance. Such were Willarski and even the Grand Master of the principal lodge. Finally, to the fourth category also a great many Brothers belonged, particularly those who had lately joined. These according to Pierre's observations were men who had no belief in anything, nor desire for anything, but joined the Freemasons merely to associate with the wealthy young Brothers who were influential through their connections or rank, and of whom there were very many in the lodge.Pierre began to feel dissatisfied with what he was doing. Freemasonry, at any rate as he saw it here, sometimes seemed to him based merely on externals. He did not think of doubting Freemasonry itself, but suspected that Russian Masonry had taken a wrong path and deviated from its original principles. And so toward the end of the year he went abroad to be initiated into the higher secrets of the order.What is to be done in these circumstances? To favor revolutions, overthrow everything, repel force by force?No! We are very far from that. Every violent reform deserves censure, for it quite fails to remedy evil while men remain what they are, and also because wisdom needs no violence. \"But what is there in running across it like that?\" said Ilagin's groom. \"Once she had missed it and turned it away, any mongrel could take it,\" Ilagin was saying at the same time, breathless from his gallop and his excitement."

    val expected = List("In the third category he included those Brothers (the majority) who saw nothing in Freemasonry but the external forms and ceremonies, and prized the strict performance of these forms without troubling about their purport or significance.",
      "Such were Willarski and even the Grand Master of the principal lodge.",
      "Finally, to the fourth category also a great many Brothers belonged, particularly those who had lately joined.",
      "These according to Pierre's observations were men who had no belief in anything, nor desire for anything, but joined the Freemasons merely to associate with the wealthy young Brothers who were influential through their connections or rank, and of whom there were very many in the lodge.",
      "Pierre began to feel dissatisfied with what he was doing.",
      "Freemasonry, at any rate as he saw it here, sometimes seemed to him based merely on externals.",
      "He did not think of doubting Freemasonry itself, but suspected that Russian Masonry had taken a wrong path and deviated from its original principles.",
      "And so toward the end of the year he went abroad to be initiated into the higher secrets of the order.",
      "What is to be done in these circumstances?",
      "To favor revolutions, overthrow everything, repel force by force?",
      "No!",
      "We are very far from that.",
      "Every violent reform deserves censure, for it quite fails to remedy evil while men remain what they are, and also because wisdom needs no violence.",
      "\"But what is there in running across it like that?\" said Ilagin's groom.",
      "\"Once she had missed it and turned it away, any mongrel could take it,\" Ilagin was saying at the same time, breathless from his gallop and his excitement.")

    val result = FromParagraphsToSentences.toSentences(input)
    assert(result === expected)
  }

  "From Paragraphs to Sentences" should "pass test case 2" in {
    val input = "In dealing with this period they sternly condemn the historical personages who, in their opinion, caused what they describe as the reaction. All the well-known people of that period, from Alexander and Napoleon to Madame de Stael, Photius, Schelling, Fichte, Chateaubriand, and the rest, pass before their stern judgment seat and are acquitted or condemned according to whether they conduced to progress or to reaction.  According to their accounts a reaction took place at that time in Russia also, and the chief culprit was Alexander I, the same man who according to them was the chief cause of the liberal movement at the commencement of his reign, being the savior of Russia.  There is no one in Russian literature now, from schoolboy essayist to learned historian, who does not throw his little stone at Alexander for things he did wrong at this period of his reign. What do these reproaches mean?  Do not the very actions for which the historians praise Alexander I (the liberal attempts at the beginning of his reign, his struggle with Napoleon, the firmness he displayed in 1812 and the campaign of 1813) flow from the same sources--the circumstances of his birth, education, and life--that made his personality what it was and from which the actions for which they blame him (the Holy Alliance, the restoration of Poland, and the reaction of 1820 and later) also flowed?  In what does the substance of those reproaches lie?  It lies in the fact that an historic character like Alexander I, standing on the highest possible pinnacle of human power with the blinding light of history focused upon him; a character exposed to those strongest of all influences: the intrigues, flattery, and self-deception inseparable from power; a character who at every moment of his life felt a responsibility for all that was happening in Europe; and not a fictitious but a live character who like every man had his personal habits, passions, and impulses toward goodness, beauty, and truth--that this character--though not lacking in virtue (the historians do not accuse him of that)--had not the same conception of the welfare of humanity fifty years ago as a present-day professor who from his youth upwards has been occupied with learning: that is, with books and lectures and with taking notes from them. \"What a woman--oh, what a woman!\" cried the King of Bohemia, when we had all three read this epistle.  \"You could not possibly have come at a better time, my dear Watson,\" he said cordially. 'It is not worth your while to wait,' she went on. 'You can pass through the door; no one hinders.' And then, seeing that I smiled and shook my head, she suddenly threw aside her constraint and made a step forward, with her hands wrung together."

    val expected = List("In dealing with this period they sternly condemn the historical personages who, in their opinion, caused what they describe as the reaction.",
      "All the well-known people of that period, from Alexander and Napoleon to Madame de Stael, Photius, Schelling, Fichte, Chateaubriand, and the rest, pass before their stern judgment seat and are acquitted or condemned according to whether they conduced to progress or to reaction.",
      "According to their accounts a reaction took place at that time in Russia also, and the chief culprit was Alexander I, the same man who according to them was the chief cause of the liberal movement at the commencement of his reign, being the savior of Russia.",
      "There is no one in Russian literature now, from schoolboy essayist to learned historian, who does not throw his little stone at Alexander for things he did wrong at this period of his reign.",
      "What do these reproaches mean?",
      "Do not the very actions for which the historians praise Alexander I (the liberal attempts at the beginning of his reign, his struggle with Napoleon, the firmness he displayed in 1812 and the campaign of 1813) flow from the same sources--the circumstances of his birth, education, and life--that made his personality what it was and from which the actions for which they blame him (the Holy Alliance, the restoration of Poland, and the reaction of 1820 and later) also flowed?",
      "In what does the substance of those reproaches lie?",
      "It lies in the fact that an historic character like Alexander I, standing on the highest possible pinnacle of human power with the blinding light of history focused upon him; a character exposed to those strongest of all influences: the intrigues, flattery, and self-deception inseparable from power; a character who at every moment of his life felt a responsibility for all that was happening in Europe; and not a fictitious but a live character who like every man had his personal habits, passions, and impulses toward goodness, beauty, and truth--that this character--though not lacking in virtue (the historians do not accuse him of that)--had not the same conception of the welfare of humanity fifty years ago as a present-day professor who from his youth upwards has been occupied with learning: that is, with books and lectures and with taking notes from them.",
      "\"What a woman--oh, what a woman!\" cried the King of Bohemia, when we had all three read this epistle.",
      "\"You could not possibly have come at a better time, my dear Watson,\" he said cordially.",
      "'It is not worth your while to wait,' she went on. 'You can pass through the door; no one hinders.",
      "' And then, seeing that I smiled and shook my head, she suddenly threw aside her constraint and made a step forward, with her hands wrung together.")

    val result = FromParagraphsToSentences.toSentences(input)
    //println(expected.diff(JavaConversions.asScalaBuffer(result).toList))
    println(expected.diff(result))
    assert(result === expected)
  }

}