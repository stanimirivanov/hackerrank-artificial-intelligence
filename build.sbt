name := "hackerrank-artificial-intelligence"

version := "1.0"

scalaVersion := "2.11.4"

scalacOptions ++= Seq("-deprecation", "-feature")

libraryDependencies += "nz.ac.waikato.cms.weka" % "weka-stable" % "3.6.12"

libraryDependencies += "edu.stanford.nlp" % "stanford-corenlp" % "3.4.1"

libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.0" % "test"

libraryDependencies += "junit" % "junit" % "4.12" % "test"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.12.1" % "test"

libraryDependencies += "org.spire-math" %% "spire" % "0.9.0" % "test"